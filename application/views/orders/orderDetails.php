
<h1>Northwind Order Viewer</h1>
<table  class="table-single-order" >   
    <thead>
        <tr>
            <th class ='head1'>Order id</th>
            <th class ='head2'>Customer Code</th>
            <th class ='head3'>Customer Name</th>
            <th class ='head4'>Served by Employee</th>
            <th class ='head5'>Order Date</th>
            <th class ='head6'>Required Date</th>
            <th class ='head7'>Shipped Date</th>
            <th class ='head8'>Ship Via</th>
            <th class ='head9'>Freight Cost</th>
        </tr>
    </thead>
    <tbody>
        <tr>    
            <td colspan="9">
                <div class="single-table-wrap" >
                    <table class="single-table-dalam">                                
                        <tbody>
                            <?php
                            $order_url = site_url("orders/$order->id");
                            $html = "<tr>"
                                    . "<td class='td-element1'> "
                                    . "<a href='$order_url'>"
                                    . "$order->orderID</a></td>"
                                    . "<td class='td-element2'>  "
                                    . "$order->code </td>"
                                    . "<td class='td-element3'>  "
                                    . "$order->companyName </td>"
                                    . "<td class='td-element4'>  "
                                    . "$order->firstName $order->lastName </td>"
                                    . "<td class='td-element5'>  "
                                    . "$order->orderDate </td>"
                                    . "<td class='td-element6'>  "
                                    . "$order->requiredDate </td>"
                                    . "<td class='td-element7'>  "
                                    . "$order->shippedDate </td>"
                                    . "<td class='td-element8'>  "
                                    . "$order->shipperName </td>"
                                    . "<td class='td-element9'>  "
                                    . "$order->freight </td>"
                                    . "</tr>\n";
                            echo $html;
                            ?>
                        </tbody>
                    </table>
                </div>
            </td>
        </tr>
    </tbody>
</table>
<h2> Order Details: </h2>
<table  class='ODtable-order' >   
    <thead>
        <tr>
            <th class ='ODH1'>Product ID</th>
            <th class ='ODH2'>Product Name</th>
            <th class ='ODH3'>Quantity</th>
            <th class ='ODH4'>Discount</th>
            <th class ='ODH5'>Quantity Per Unit</th>
            <th class ='ODH6'>Unit Price</th>
            <th class ='ODH7'>Supplier Name</th>
        </tr>
    </thead>
    <tbody>
        <tr>    
            <td colspan="7">
                <div class="ODtable-wrap" >
                    <table class="ODtable-dalam">                                
                        <tbody>
                            <?php
                            foreach ($orderlist as $id => $detail) {
                                $html = "<tr>"
                                        . "<td class='td-OD1'>  "
                                        . "$detail->ProductID </td>"
                                        . "<td class='td-OD2'>  "
                                        . "$detail->ProductName </td>"
                                        . "<td class='td-OD3'>  "
                                        . "$detail->Quantity</td>"
                                        . "<td class='td-OD4'>  "
                                        . "$detail->Discount </td>"
                                        . "<td class='td-OD5'>  "
                                        . "$detail->QuantityPerUnit </td>"
                                        . "<td class='td-OD6'>  "
                                        . "$detail->UnitPrice </td>"
                                        . "<td class='td-OD7'>  "
                                        . "$detail->SupplierName </td>"
                                        . "</tr>\n";
                                echo $html;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </td>
        </tr>
    </tbody>
</table>