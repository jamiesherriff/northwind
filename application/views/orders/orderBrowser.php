<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define('COMBO_SIZE', 20);  // Number of lines to display

/*
 * Return the HTML for a div containing a label, a combo box and a 'go'
 * button. 
 * @param string $label - text to display as a label
 * @param assoc-array $map - a map from value to name
 * @param int $selectedRowValue - the value of the currently-selected option
 * @param int $size - the number of elements to display
 * @return - an html string for display
 */


?>
<h1>Northwind Order Browser</h1>
            <table  class='table-order' >   
                <thead>
                        <tr>
        <th class ='head1'>Order id</th>
        <th class ='head2'>Customer Code</th>
        <th class ='head3'>Customer Name</th>
        <th class ='head4'>Served by Employee</th>
        <th class ='head5'>Order Date</th>
        <th class ='head6'>Required Date</th>
        <th class ='head7'>Shipped Date</th>
        <th class ='head8'>Ship Via</th>
        <th class ='head9'>Freight Cost</th>
    </tr>
                </thead>
                <tbody>
                    <tr>    
           <td colspan="9">
      <div class="table-wrap" >
      <table class="table-dalam">
                                
                <tbody>
                <?php
                foreach ($orders as $id => $order) {
                    $order_url = site_url("orders/$order->id");
                    $html = "<tr>"
                            . "<td class='td-element1'> "
                            . "<a href='$order_url'>$order->id</a></td>"
                            . "<td class='td-element2'>  "
                            . "$order->code </td>"
                            . "<td class='td-element3'>  "
                            . "$order->companyName </td>"
                            . "<td class='td-element4'>  "
                            . "$order->firstName $order->lastName </td>"
                            . "<td class='td-element5'>  "
                            . "$order->orderDate </td>"
                            . "<td class='td-element6'>  "
                            . "$order->requiredDate </td>"
                            . "<td class='td-element7'>  "
                            . "$order->shippedDate </td>"
                            . "<td class='td-element8'>  "
                            . "$order->shipperName </td>"
                            . "<td class='td-element9'>  "
                            . "$order->freight </td>"
                            . "</tr>\n";
                    echo $html;
                }
                ?>
      </tbody>
       </table>
     </div>
   </td>
 </tr>
  </tbody>
 </table>