<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $title; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="<?php echo base_url("styles/nwstyles.css"); ?>" 
              rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-container">
            <!-- site banner and Message Banner -->
            <div class="banner clearfix">
                <div class="banner-content clearfix">             
                </div>
            </div>
            <ul class="menu">
                <li><a href="<?php echo site_url('products/') ?>">
                        <span>Product browser</span></a></li>
                <li><a href="<?php echo site_url('orders/') ?>">
                        <span>Order Browser</span></a></li>
                <li><a href="<?php echo site_url('about/') ?>">
                        <span>About</span></a></li>
                <li><a href="<?php echo site_url('home/') ?>">
                        <span>User</span></a></li>
            </ul>
            <div class= "main-content">
                <?php echo $content; ?>
            </div>
            <div class="footer">
                <p style="font-weight:bold">&copy; Jamie Sherriff 2014</p>
            </div>
        </div>
    </body>
</html>