<h2> About Page </h2>
<p>This page documents what my Northwind site does,  known bugs,  important 
 design decisions, and any other important  usage  information markers or 
 users require.</p>
 
<p><b>Login:</b><br />
Any Northwind employee listed in the employees table can login using their 
first name as the username and last name as the password.<br />
An example login would be username: <i>Andrew</i> password: <i>Fuller</i><br />
If two employees happen to have the same first and last name there 
is no handling of which one will be chosen.<br />
Usernames are case insensitive and passwords are case sensitive.<br />
Visitors are not visually identified and tracked as it assumed users are 
Northwind employees and will login.
</p> 

<p><b>Product Browser:</b><br />
This page has been untouched aside from the lab work to fix the bugs.</p>
 
<p><b>UI:</b><br />
The order table has been limited to 960 pixels for a compromise between 
readability and size. This means any browser width resolution less than 960 
gets a horizontal scroll bar.<br/>
CSS has been used to align tables, content and pictures etc.
</p>
 
<p><b>Order Browser:</b><br />
This page shows all of the orders in the Northwind orders table.<br />
It was decided that a html table in list form would be used over a combo box for 
readability and easily displaying information into columns. Ideally 
this would have been nicer to do in JavaScript.<br />
Orders are ordered by descending order id. This uses the assumption orders 
a created sequentially and the biggest number is the most recent order. 
This means the most recent order is first in the list.<br />
Sorting the orders by date returned orders in different 
ordering because a lot of orders have the same date.<br />
The order information has been joined to other tables to show other useful
information such as the customer name and code and the name of the shipper</p>
 
<p><b>Order Details:</b><br />
The details page is loaded by clicking the respective hyperlink in the order 
browser list.<br />
The information has been joined to other tables to show other useful 
information such as the product name and id, quantity per unit and the supplier
name of the product.</p>
 
<p><b>Security:</b><br />
I have followed the Code Ignighter database model so everything has been 
escaped.<br />
XSS attacks have also been accounted for by using xss_clean on my input 
fields.<br />
The user cannot access any page except the about page without logging in. 
This was done by writing my own controller class that extends CI controller 
and just checks if the user has logged in and redirects if not.<br />
This also includes trying to directly access the controller and its method 
such as /index.php/orders/browser<br />
Global_xss_filtering and csrf_protection have also been enabled in Code 
Ignighter to provide more security.<br />
</p>
 
<p><b>User:</b><br />
A simple user page was setup to show the user who is logged in and if it 
is not them logout and re login as them.<br />
This means multiple employees can use the same browser without needing to 
close it.</p>
 
<p><b>Session Management:</b><br />
Cookies are set to expire on the browser closing.<br />
The PHP function site_url has been used to allow portability in HTML href links. 
</p>
 
<p><b>Known Bugs:</b><br />
Visual issues occur between IE, Firefox and chrome. An example is the table 
text for the order browser overflows to a new line in Chrome and IE but 
does not in Firefox.<br/>
The development environment has been left enabled in Code Ignighter to show
to the marker that no PHP errors exist.
</p>
 
<p><b>Source list:</b><br />
Source for the Northwind logo: 
http://www.northwindinc.com/nwlogo2008_files/backgroundchange1.png<br />
</p>