<?php

/* Custom Controller to handle login check
 * Internal pages that require login authentication require this view
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_LoginController extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   session_start();
  $this->load->helper('url');
      if(!$this->session->userdata('logged_in'))
   {
       redirect('login','refresh');
       }
 }
}