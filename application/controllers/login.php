<?php

/*
 * Login controller that controls access to the login page
 * Has no login restriction and has public access
 * Extends CI_Controller
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    function index() {
        $this->load->helper('url');
        $this->load->helper(array('form'));
        $data['title'] = ucfirst('login');
        $data['content'] = $this->load->view(
                'login/loginView', $data, TRUE);
        $this->load->view('templates/master', $data);
    }
}
