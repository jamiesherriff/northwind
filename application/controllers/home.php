<?php

/*
 * Home controller that controls access to the user page view
 * Has login restrictions and must be  a valid user that is logged in
 * Extends MY_LoginController
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends MY_LoginController {

    function index() {
        $this->load->helper('url');
        if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['fullname'] = $session_data['fullname'];
            $data['title'] = ucfirst('User Page');
            $data['content'] = $this->load->view(
                    'home/homeView', $data, TRUE);
            $this->load->view('templates/master', $data);
        } else {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
 
    public function logout() {
        $this->load->helper('url');
        //extra check incase user got out of sequence and try to destory nothing
        if (!isset($_SESSION)) {
            session_start();
        }
        $this->session->unset_userdata('logged_in');
        session_destroy();
        redirect('home', 'refresh');
    }
}
?>

