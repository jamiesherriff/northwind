<?php

/*
 * Products controller that controls access to the product Browser page
 * Has login restrictions and must be  a valid user that is logged in
 * Extends MY_LoginController
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Products extends MY_LoginController {
    
    public function index() {
        // Default method for this controller
        $this->load->helper('url');
        if ($this->session->userdata('logged_in')) {
            $this->browser();
            //Not logged in, redirect to a public page
        } else {
            redirect('home', 'refresh');
        }
    }

    public function browser() {
        $this->load->model('category');
        $this->load->model('product');
        $this->load->helper('url');
        $categoryMap = $this->category->listAll();
        $currentCategoryId = $this->input->get('Category');
        if (!$currentCategoryId) {
            $currentCategoryId = key($categoryMap);
        }
        $productMap = $this->product->listAll($currentCategoryId);
        $currentProductId = $this->input->get('Product');
        if (!$currentProductId || 
            !array_key_exists($currentProductId,$productMap)) {
                $currentProductId = key($productMap);
        }
        $product = $this->product->read($currentProductId);
        $title = ucfirst('Northwind Products');
        $data = array('categoryMap' => $categoryMap,
            'productMap' => $productMap,
            'product' => $product,
            'title' => 'Northwind Products');
        $data['content'] = $this->load->view(
                'products/productBrowser', $data, TRUE);
        $this->load->view('templates/master', $data);
    }
}
