<?php

/*
 * Orders controller that controls access to the Order Browser and
 * Order Details pages
 * Has login restrictions and must be  a valid user that is logged in
 * Extends MY_LoginController
 * 
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Orders extends MY_LoginController {

    public function index() {
        // Default method for this controller
        $this->load->helper('url');
        if ($this->session->userdata('logged_in')) {
//Not logged in, redirect to a public page
            $this->browser();
        } else {
            redirect('home', 'refresh');
        }
    }
 
    public function browser($page = 'orderBrowser') {
        if (!file_exists(APPPATH . '/views/orders/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $data['title'] = ucfirst('Orders');
        $this->load->model('order');
        $this->load->helper('url');
        $orders = $this->order->list_all();
        $data = array('orders' => $orders,
            'title' => 'Northwind Orders');
        $data['content'] = $this->load->view(
                'orders/orderBrowser', $data, TRUE);
        $this->load->view('templates/master', $data);
    }
    
    /*
 * Load the respective order details page
 * @param integer of the order number to display
 */
    public function details($order_number = False, $page = 'orderDetails') {
        if (!file_exists(APPPATH . '/views/orders/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        if (!$order_number) {
            show_404();
        }
        $data['title'] = 'Orders';
        $this->load->model('order');
        $this->load->helper('url');
        $order = $this->order->get_single_order($order_number);
        if (!$order) {
            show_404();
        } 
        $detail_list = $this->order->get_details($order_number);
        $data = array('order' => $order,
                'orderlist' => $detail_list,
                'title' => 'Northwind Products');
        $data['content'] = $this->load->view(
                'orders/orderDetails', $data, TRUE);
        $this->load->view('templates/master', $data);
    }

}
