<?php

/*
 * About controller that controls access to the about page view
 * Has no login restriction and has public access
 * Extends CI_Controller
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class About extends CI_Controller {

    public function index() {  // Default method for this controller
        viewer();
    }

    public function viewer($page = 'about') {
        if (!file_exists(APPPATH . '/views/pages/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $data['title'] = ucfirst($page);
        $this->load->helper('url');
        $data['content'] = $this->load->view(
                'pages/about', $data, TRUE);
        $this->load->view('templates/master', $data);
    }
}
