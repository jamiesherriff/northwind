<?php
/*
 * Declare the Order class, representing rows of the Order table.
 * Uses a listAll function to get all orders in the order table.
 * Has a seperate function to check if a single order exists 
 * Another function to retrieve all order details for a specific order.
 * This class uses CodeIgniter's 
 */
class Order extends CI_Model {

    public $id;
    public $customerID;
    public $employeeID;
    public $orderDate;
    public $code;
    public $requiredDate;
    public $shippedDate;
    public $shipVia;
    public $freight;
    public $firstName;
    public $lastName;
    public $companyName;
    
    public function __construct() {
        $this->load->database();
    }

    public function list_all() {
        $this->db->select("Orders.*, Orders.id AS OrderID");
        $this->db->select("Shippers.CompanyName AS ShipperName");
        $this->db->select("Customers.Code,Customers.CompanyName");
        $this->db->select("Employees.FirstName,Employees.LastName");
        $this->db->from("Orders");
        $this->db->join("Shippers", "Shippers.id = Orders.ShipVia");
        $this->db->join("Customers", "Customers.id = Orders.CustomerID");
        $this->db->join("Employees", "Employees.id = Orders.EmployeeID");
        $this->db->order_by('id', "desc");
        $order = new Order();
        $rows = $this->db->get()->result();
        $this->checkResult($rows);
        foreach ($rows as $row) {
            $order = new Order();
            $order->load($row);
            $list[] = $order;
        }
        return $list;
    }

    public function get_single_order($orderId = False) {
        if ($orderId === FALSE) {
            $query = $this->db->get('Orders');
            return $query->result_array();
        }
        $this->db->select("Orders.*, Orders.id AS OrderID");
        $this->db->select("Shippers.CompanyName AS ShipperName");
        $this->db->select("Customers.Code,Customers.CompanyName");
        $this->db->select("Employees.FirstName,Employees.LastName");
        $this->db->from("Orders");
        $this->db->join("Shippers", "Shippers.id = Orders.ShipVia");
        $this->db->join("Customers", "Customers.id = Orders.CustomerID");
        $this->db->join("Employees", "Employees.id = Orders.EmployeeID");
        $this->db->where(array('Orders.Id' => $orderId));
        $order = new Order();
       
        $rows = $this->db->get();
        $this->checkResult($rows);
        if($rows->num_rows !== 1){
            return FALSE;
        }
        $rows = $rows->result();

        //$rows = $this->db->get()->result();
        
        $row = $rows[0];
        $order->load($row);
        return $order;
    }

    public function get_details($orderId = False) {
        if ($orderId === FALSE) {
            $query = $this->db->get('OrderDetails');
            return $query->result_array();
        }
        $this->db->select('OrderDetails.*');
        $this->db->select('Products.ProductName,Products.QuantityPerUnit,'
                . 'Products.UnitPrice');
        $this->db->select('Suppliers.CompanyName AS SupplierName');
        $this->db->from('OrderDetails');
        $this->db->where(array('OrderDetails.orderId' => $orderId));
        $this->db->join("Products", "Products.id = OrderDetails.ProductID");
        $this->db->join('Suppliers', 'Products.SupplierID = Suppliers.id');
        $rows = $this->db->get();
        $this->checkResult($rows);
        //
        if($rows->num_rows < 1){
            return FALSE;
        }
        $rows = $rows->result();

        
        return $rows;
    }

    private function load($row) {
        foreach ((array) $row as $field => $value) {
            $fieldName = strtolower($field[0]) . substr($field, 1);
            $this->$fieldName = $value;
        }
    }

    // Check that the result from a DB query was OK
    private static function checkResult($result) {
        global $DB;
        if (!$result) {
            //var_dump($result);
            die("DB error ({$DB->error})");
        }
    }

}
